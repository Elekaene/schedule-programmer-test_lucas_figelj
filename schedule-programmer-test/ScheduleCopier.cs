﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;

namespace SIPlay
{
    /// <summary>
    /// Instructions...
    /// 
    /// This file contains a test, 3 entities and an interface.
    /// 
    /// Schedule,Events and Teams are the three entities.  You will implement 
    /// a ScheduleCopier that performs a deep copy of a Schedule and its Events and Teams.
    /// 
    /// Sucessfully completing the test means that the single CopySchedule test passes.
    /// 
    /// 1) Implement the copy method of ScheduleCopier.
    /// 2) Runs tests.
    /// 3) Tests failed? GOTO step 1;
    /// 4) Profit
    /// </summary>
    public class ScheduleCopier
    {
        public Schedule Copy(Schedule schedule)
        {
            //This implementation can be moved to a static method and also the Schedule type can be changed to T type in order to be used for any kind of object types.
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, schedule);
                stream.Position = 0;

                return (Schedule)formatter.Deserialize(stream);
            }
        }       
    }

    [TestFixture]
    public class Tests
    {       
        public Schedule CreateSchedule()
        {
            var manu = new Team() {Id = 1, Name = "Manchester United"};
            var arsenal = new Team() {Id = 2, Name = "Arsenal"};

            var gameOne = new Event(1, "Manchester United vs Arsenal", manu, arsenal);
            var gameTwo = new Event(2, "Arsenal vs Manchester United", arsenal, manu);

            manu.Events.AddRange(new[] {gameOne, gameTwo});
            arsenal.Events.AddRange(new[] {gameOne, gameTwo});

            return new Schedule(1, "Fall Schedule", new List<Event>() {gameOne, gameTwo});
        }

        [Test]
        public void CopySchedule()
        {
            var schedule = CreateSchedule();
            var copy = new ScheduleCopier().Copy(schedule);

            var firstGame = copy.Events.First();
            var secondGame = copy.Events.Last();

            Assert.IsFalse(object.ReferenceEquals(schedule, copy), "You tried to trick me...");
            Assert.IsFalse(object.ReferenceEquals(firstGame, schedule.Events.First()), "Events not actually copied");
            Assert.IsFalse(object.ReferenceEquals(firstGame.Home, schedule.Events.First().Home),
                "Team was not actually copied");
            Assert.AreEqual(2, copy.Events.Count(), "Should have been 2 events on the schedule");
            Assert.AreEqual(schedule.Name, copy.Name);
            Assert.AreEqual(1, firstGame.Id, "First game should have had Id 1");
            Assert.AreEqual(2, secondGame.Id, "Second game should have had id 2");
            Assert.AreEqual("Manchester United vs Arsenal", firstGame.Title, "First game had the wrong title");
            Assert.AreEqual("Manchester United", firstGame.Home.Name, "First game had wrong home team name");
            Assert.AreEqual(2, firstGame.Home.Events.Count, "The home team should have had two events");
            Assert.IsTrue(object.ReferenceEquals(firstGame.Home, secondGame.Away),
                "The home team of first game should be the same instance as the away team from the second game");
            Assert.IsTrue(object.ReferenceEquals(firstGame.Home.Events.First(), firstGame),
                "The home team should hold the same instance as its first game as the schedule");
        }
    }

    [Serializable]
    public abstract class Entity
    {
        public int Id { get; set; }
    }

    [Serializable]
    public class Event : Entity
    {
        public Event(int id, string title, Team home, Team away)
        {
            Console.WriteLine("New Event:{0}", id);
            Id = id;
            Title = title;
            Home = home;
            Away = away;
            Start = DateTime.Now.AddDays(1);
        }

        public string Title { get; set; }

        public DateTime Start { get; set; }

        public Team Home { get; set; }

        public Team Away { get; set; }
    }

    [Serializable]
    public class Schedule : Entity
    {
        public Schedule()
        {
            Console.WriteLine("New Schedule");
            Events = new List<Event>();
        }

        public Schedule(int id, string name, List<Event> events)
        {
            Console.WriteLine("New Schedule:{0}", id);
            Id = id;
            Name = name;
            Events = events;
        }

        public string Name { get; set; }

        public IEnumerable<Event> Events { get; private set; }

    }

    [Serializable]
    public class Team : Entity
    {
        public Team()
        {
            Console.WriteLine("New Team");
            Events = new List<Event>();
        }

        public string Name { get; set; }

        public List<Event> Events { get; private set; }
    }
}